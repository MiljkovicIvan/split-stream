#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <errno.h>
#include <new>

#include <linux/stat.h>

using namespace std;

//buffer size is 16M
#define BUFFER_SIZE 16*1024*1024
#define BUFFER_CLOSED -1

#define VERBOSE


volatile static int end = 0;

volatile int *parentWaiting = 0;

class BoundedBuffer {
public:

    BoundedBuffer() {
        items = 0;
        head = tail = 0;
        closed = 0;
    }

    void append(char c) {
#ifdef VERBOSE
        printf("[append] %d\n", items);
#endif
        while(items == BUFFER_SIZE); // wait for space
        buffer[tail] = c;
        tail = (tail+1) % BUFFER_SIZE;
        items++;
    }

    char take() {

#ifdef VERBOSE
        printf("[take] %d\n", items);
#endif

        while(items == 0) {
            if(this->closed) return BUFFER_CLOSED;
        }; //wait for some items
        char c = buffer[head];
        head = (head+1) % BUFFER_SIZE;
        items--;
        return c;
    }

    int empty() {
        if (items == 0)
            return 1;
        return 0;
    }

    void close() {
        closed = 1;
    }

private:
    volatile int items;
    int head, tail;
    char buffer[BUFFER_SIZE];
    int closed;

};

BoundedBuffer* buffer = 0;

void write_to_pipes(const char* pipe1, const char* pipe2) {
    FILE* f1;
    FILE* f2;

    if (((f1 = fopen(pipe1, "wb")) == NULL) || (f2 = fopen(pipe2, "wb")) == NULL) {
            fprintf(stderr, "failed opening output fifos");
            exit(1);
    }

    char c;
    c = buffer->take();
    if(c == BUFFER_CLOSED) {
        fclose(f1);
        fclose(f2);
        return;
    }

    //fputc(c, f1);
    //fputc(c, f2);

    fread(&c, 1, 1, f1);
    fread(&c, 1, 1, f2);

    fclose(f1);
    fclose(f2);
}

void read_from_pipe() {
    char c;

    //while((c = getc(stdin)) != EOF) {
        //buffer->append(c);
    //}

    while(!feof(stdin)) {
        fread(&c, 1,1,stdin);
        buffer->append(c);
    }

    buffer->close();

}

int main(int argc, char ** argv) {

    if (argc != 3) {
        fprintf(stderr, "Invalid input.");
        exit(EXIT_FAILURE);
    }

    pid_t pid;

    buffer = (BoundedBuffer*)mmap(0, sizeof(BoundedBuffer),
                                  PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,
                                  -1, 0);
    parentWaiting = (int*)mmap(0, sizeof(int),
                                  PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,
                                  -1, 0);
    *parentWaiting = 0;

    freopen(NULL, "rb", stdin);

    if(buffer == MAP_FAILED) {
        printf("Error mmaping the buffer.");
        exit(EXIT_FAILURE);
    }
    if(parentWaiting == MAP_FAILED) {
        printf("Error mmaping the parentWaiting.");
        exit(EXIT_FAILURE);
    }

    new(buffer) BoundedBuffer();

    /*creating two processes*/
    /*child process to write to pipes*/
    /*parent process to read from input pipe*/


    pid = fork();

    while(1) {
        if (pid == 0) {
            /*child process*/
            //printf("----------------------------------------\n");
            write_to_pipes(argv[1], argv[2]);
            if ((*parentWaiting == 1) && (buffer->empty())) {
#ifdef VERBOSE
                printf("child finished");
#endif
                munmap((int*)parentWaiting, sizeof(int));
                break;
            }
        }
        else if (pid < (pid_t) 0){
            /*fork failed*/
            fprintf(stderr, "fork failed.\n");
            return EXIT_FAILURE;
        }
        else {
            /*parent process*/
            read_from_pipe();
#ifdef VERBOSE
            printf("parent waiting\n");
#endif
            *parentWaiting = 1;
            wait(NULL); // wait of child process to finish
            return 0;
        }
    }

    munmap(buffer, sizeof(BoundedBuffer));

    return 0;
}

