#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

#define BUFFER_SIZE 5



int main(int argc, char** argv) {
    int fd1, fd2;

    freopen(NULL, "rb", stdin);

    fd1 = open("../fifo1", O_WRONLY);
    fd2 = open("../fifo2", O_WRONLY);

    char buffer[BUFFER_SIZE];

    while(!feof(stdin)) {


        size_t numbytes = fread(buffer, 1, BUFFER_SIZE, stdin);

        write(fd1, buffer, numbytes);
        write(fd2, buffer, numbytes);


    }

    close(fd1);
    close(fd2);

}

