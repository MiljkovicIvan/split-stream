#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/stat.h>

#define FIFO_FILE "../fifo1"
#define buffer_size 1

int main() {
    FILE* fp;
    char c;

    freopen(NULL, "wb", stdout);

    umask(0);
    mknod(FIFO_FILE, S_IFIFO|0666, 0);

    while(1) {
        fp = fopen(FIFO_FILE, "rb");

        size_t numbytes = fread(&c, 1, 1, fp);
        fwrite(&c, 1, numbytes, stdout);
        printf("%c", c);

        fclose(fp);
    }

    return 0;
}

