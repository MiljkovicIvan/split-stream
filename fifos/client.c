#include <stdio.h>
#include <stdlib.h>

#define fifo1 "../fifo1"
#define fifo2 "../fifo2"

#define BUFFER_SIZE 100

int main(int argc, char* argv[]) {
    FILE* f1, *f2;

    freopen(NULL, "rb", stdin);

    char buffer[BUFFER_SIZE];

    while(!feof(stdin)) {
        f1 = fopen(fifo1, "wb");
        f2 = fopen(fifo2, "wb");
        size_t numbytes = fread(buffer, 1, BUFFER_SIZE, stdin);

        fwrite(buffer, 1, BUFFER_SIZE, f1);
        fwrite(buffer, 1, BUFFER_SIZE, f2);

        fclose(f1);
        fclose(f2);

        for(int i = 0; i < 1000000; i++);

    }


    return 0;
}

